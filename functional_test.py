from selenium import webdriver
import unittest, time
from selenium.webdriver.common.keys import Keys
from datetime import datetime

class VisitorTest(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Chrome(executable_path='/usr/bin/chromedriver')
    def test_isi(self):
        driver = self.browser
        driver.get('http://localhost:8000/')
        elem = driver.find_element_by_name('message')
        elem.clear()
        elem.send_keys("halo")
        elem.send_keys(Keys.RETURN)
        assert "halo" in driver.page_source
        time.sleep(5)
        assert datetime.now().strftime('%h. %d, %Y,') in driver.page_source
    def tearDown(self):
        self.browser.quit()

if __name__ == "__main__":
    unittest.main()