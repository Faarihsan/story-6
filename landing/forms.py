from django import forms

class CreateMessage(forms.Form):
    message = forms.CharField(label='message', widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'name' : 'message',
        'type' : 'text',
    }), max_length=300)