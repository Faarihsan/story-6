# Generated by Django 2.2.6 on 2019-10-29 14:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0002_auto_20191029_1408'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='date',
            field=models.TimeField(),
        ),
    ]
