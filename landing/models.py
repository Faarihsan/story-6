from django.db import models

class Message(models.Model):
    msg = models.TextField(max_length=300)
    date = models.DateTimeField(auto_now_add=True)