import os
from django.test import TestCase, Client
from django.urls import resolve
from .views import landing
from .models import Message
from .forms import CreateMessage
from datetime import datetime
from selenium import webdriver
import unittest, time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from datetime import date
from story6.settings import BASE_DIR

class Story6UnitTest(TestCase):
    def test_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    def test_method_exist(self):
        found  = resolve('/')
        self.assertEqual(found.func, landing)
    def test_panggil_template_yang_benar(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landing/landing.html')
    def test_ada_tulisan(self):
        response = Client().get('/')
        self.assertContains(response, "Halo, Apa kabar?")
    def test_add_message(self):
        data = {'message' : 'halo'}
        response = Client().post('/', data)
        self.assertEqual(response.status_code, 302)
    def test_models(self):
        data = Message(msg = 'hyahya')
        data.save()
        response = Client().get('/')    
        self.assertContains(response, 'hyahya')
    def test_form(self):
        data = {'message' : 'alibaba'}
        form = CreateMessage(data=data)
        self.assertTrue(form.is_valid())

class VisitorTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(executable_path=os.path.join(BASE_DIR,'chromedriver'), chrome_options=chrome_options)
    def test_isi(self):
        driver = self.browser
        driver.get('http://localhost:8000/')
        elem = driver.find_element_by_name('message')
        elem.clear()
        elem.send_keys("coba coba")
        submit = driver.find_element_by_id('submit')
        submit.send_keys(Keys.RETURN)
        assert "coba coba" in driver.page_source
    def tearDown(self):
        self.browser.quit()