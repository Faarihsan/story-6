from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import CreateMessage
from .models import Message

def landing(request):
    form = CreateMessage()
    data = Message.objects.all().order_by('-date')
    if request.method == 'POST' :
        form  = CreateMessage(request.POST)
        if form.is_valid():
            msg = Message(
                msg = form.cleaned_data['message'],
            )
            msg.save()
            return redirect('landing:landing')
    return render(request, 'landing/landing.html', {'form':form, 'data':data})